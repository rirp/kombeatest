//
//  WalkthroughRouter.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import UIKit

class WalkthroughtRouter: WalkthroughtPresenterToRouterProtocol {
    class func getWalkthroughViewController() -> UIViewController? {
        let storyBoard = UIStoryboard(name: "Walkthrought", bundle: nil)
        let view = storyBoard.instantiateViewController(withIdentifier: "walkthroughVC") as? WalkthroughViewController
        let presenter = WalkthroughPresenter()
        let interector = WalkthroughInterector()
        
        view?.presenter = presenter
        presenter.interector = interector
        presenter.view = view
        
        return view!
    }
    
}
