//
//  MainRouter.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import UIKit

class MainRouter: MainPresenterToRouterProtocol {
    class func createModule() -> UIViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
        let presenter: MainViewToPresenterProtocol & MainInterectorToPresenterProtocol = MainPresenter()
        let interactor: MainPresentorToInterectorProtocol = MainInterector()
        let router: MainPresenterToRouterProtocol = MainRouter()
        
        view?.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interector = interactor
        interactor.presenter = presenter
        
        return view!
    }
    
    func goToWalkthrough() {
        
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
}
