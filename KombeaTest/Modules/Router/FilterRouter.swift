//
//  FilterRouter.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/6/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import UIKit

class FilterRouter {
    class func createFilterViewController(product: Products, universes: [String]) -> UIViewController? {
        let storyBoard = UIStoryboard(name: "Filter", bundle: nil)
        let view = storyBoard.instantiateViewController(withIdentifier: "filterVC") as? FilterViewController
        let presenter = FilterPresenter()
        let interector = FilterInterector()
        view?.universes = universes
        view?.productList = product
        view?.presenter = presenter
        presenter.interector = interector
        presenter.view = view
        
        return view!
    }
}
