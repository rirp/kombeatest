//
//  DetailViewController.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/4/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit


class DetailViewController: BaseViewController {
    @IBOutlet weak var skuLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var universeLabel: UILabel!
    @IBOutlet weak var typeCharacterLabel: UILabel!
    @IBOutlet weak var downloadsLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var priceLabel: DesignableLabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UITextView!
    
    var product: Result?
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let product = product else {
            return
        }
        self.navigationItem.backBarButtonItem?.title = " "
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.setNavButtonIcos()
        setRating(value: Int(product.rating)!)
        skuLabel.text = "SKU: \(product.sku)"
        nameLabel.text = product.name.uppercased()
        universeLabel.text = product.universe
        typeCharacterLabel.text = product.kind
        downloadsLabel.text = "\(product.downloads) downloads"
        priceLabel.text = "$\(String(describing: product.price))"
        if let data = product.imageData {
            imageView.image = UIImage(data: data)
        } else {
            guard let url = URL(string: product.imageURL!) else {
                return
            }
            getImageData(from: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async() {
                    self.imageView.image = UIImage(data: data)
                }
            }
        }
        descriptionLabel.text = product.description
    }
    
    func getImageData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    private func setRating(value: Int) {
        ratingView.settings.updateOnTouch = false
        ratingView.settings.fillMode = .full
        ratingView.settings.totalStars = 5
        ratingView.rating = Double(value)
    }
    
    @IBAction func sharedPressed(_ sender: Any) {
        let textShare = [product?.description]
        let excludeActivities = [UIActivityType.airDrop, UIActivityType.print, UIActivityType.assignToContact, UIActivityType.saveToCameraRoll, UIActivityType.addToReadingList, UIActivityType.postToFlickr, UIActivityType.postToVimeo, UIActivityType.postToFacebook, UIActivityType.message, UIActivityType.postToWeibo]
        let activityViewController = UIActivityViewController(activityItems: textShare,
                                                              applicationActivities: nil)
        activityViewController.excludedActivityTypes = excludeActivities
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
