//
//  BaseViewController.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    var activityIndicator: UIActivityIndicatorView?
    var loadingView: UIView?
    var swipeGest: UISwipeGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLoadingView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let currentWindow = UIApplication.shared.keyWindow
        currentWindow?.addSubview(self.loadingView!)
        self.loadingView?.isHidden = true
    }
    
    func buildLoadingView() {
        let origin = self.view.frame.origin
        let size = UIScreen.main.bounds
        self.loadingView = UIView.init(frame: CGRect(x: origin.x,
                                                     y: origin.y,
                                                     width: size.width,
                                                     height: size.height))
        self.loadingView?.backgroundColor = UIColor.gray
        self.loadingView?.alpha = 0.6
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        self.activityIndicator?.center = CGPoint(x: size.width / 2, y: (size.height / 2) - 10)
        self.loadingView?.addSubview(activityIndicator!)
    }
    
    func showLoadingView() {
        loadingView?.isHidden = false
        activityIndicator?.startAnimating()
    }
    
    func removeLoadingView() {
        loadingView?.isHidden = true
        activityIndicator?.stopAnimating()
    }
    
    func setNavButtonIcos() {
        navigationItem.rightBarButtonItem = nil
        var rightImage = UIImage(named: "Shared_icon")
        rightImage = rightImage?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: rightImage,
                                                                 style: UIBarButtonItemStyle.plain,
                                                                 target: nil,
                                                                 action: nil)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "",
                                                           style: .plain,
                                                           target: nil,
                                                           action: nil)
    }
    
    func showMessageError(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.message.title", comment: ""),
                                      message: NSLocalizedString("error.message.connection", comment: ""),
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("message.try_again", comment: ""),
                                      style: UIAlertActionStyle.default,
                                      handler: {(alert: UIAlertAction!) in
                                            self.tryAgainAction()
                                        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tryAgainAction() {
        // empty implementation
    }
    
    func goToViewController(viewController: UIViewController) {
        self.present(viewController, animated: false, completion: nil)
    }
}

extension BaseViewController: UIGestureRecognizerDelegate {
    func addDownTapGesgure() {
        swipeGest = UISwipeGestureRecognizer(target: self,
                                             action: #selector(swipeDownRecognizer(_:)))
        swipeGest?.direction = .down
        self.view.addGestureRecognizer(swipeGest!)
        swipeGest?.delegate = self
    }
    
    func removeTapGesture() {
        guard swipeGest != nil else {
            return
        }
        self.view .removeGestureRecognizer(swipeGest!)
    }
    
    @objc func swipeDownRecognizer(_ gestureRecognizer: UISwipeGestureRecognizer) {
        // Empty implementation
    }
}
