//
//  FilterViewController.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/4/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class FilterViewController: BaseViewController {
    var presenter: FilterViewToPresenterProtocol?
    var existFilter: Bool! {
        didSet {
            if filterOption == nil {
                filterOption = FilterEntity()
            }
        }
    }
    var productList: Products?
    var filterOption: FilterEntity?
    var universes: [String]! {
        didSet {
            universes.removeFirst()
            universeOptions = [Bool]()
            universes.forEach { (_) in
                universeOptions?.append(false)
            }
        }
    }
    var previousSelection: Int = -1
    var universeOptions: [Bool]?
    
    lazy var categories: [String] = {
        return Constant.CATEGORIES
    }()
    @IBOutlet weak var dateAddedImage: UIImageView!
    @IBOutlet weak var dateAddedButton: UIButton!
    
    @IBOutlet weak var rating1ImageView: UIImageView!
    @IBOutlet weak var rating2ImageView: UIImageView!
    @IBOutlet weak var rating3ImageView: UIImageView!
    @IBOutlet weak var rating4ImageView: UIImageView!
    @IBOutlet weak var rating5ImageView: UIImageView!
    
    @IBOutlet weak var rating1Button: UIButton!
    @IBOutlet weak var rating2Button: UIButton!
    @IBOutlet weak var rating3Button: UIButton!
    @IBOutlet weak var rating4Button: UIButton!
    @IBOutlet weak var rating5Button: UIButton!
    
    @IBOutlet weak var priceImageView: UIImageView!
    @IBOutlet weak var priceButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    
    @IBOutlet weak var downLoadImageView: UIImageView!
    
    @IBOutlet weak var universesTableView: UITableView!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var leftSlider: CustomSlider!
    @IBOutlet weak var rightSlider: CustomSlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        existFilter = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupSliders()
        universesTableView.reloadData()
    }
    
    @IBAction func leftSaliderChanged(_ sender: Any) {
        existFilter = true
        if filterOption?.orderBy != .Price {
            priceButton.sendActions(for: .touchUpInside)
        }
        let value = String(format: "%.2f", leftSlider.value)
        minLabel.text = String("$\(value)")
    }
    
    @IBAction func rightSaliderChanged(_ sender: Any) {
        existFilter = true
        if filterOption?.orderBy != .Price {
            priceButton.sendActions(for: .touchUpInside)
        }
        let value = String(format: "%.2f", rightSlider.value)
        maxLabel.text = String("$\(value)")
    }
    
    @IBAction func downloadActionPressed(_ sender: Any) {
        existFilter = true
        downLoadImageView.image = UIImage(named: "ico_circle_selected")
        dateAddedImage.image = UIImage(named: "ico_circle_unselected")
        priceImageView.image = UIImage(named: "ico_circle_unselected")
        filterOption?.orderBy = .DownLoads
    }
    
    
    @IBAction func dateAddedActionPressed(_ sender: Any) {
        existFilter = true
        downLoadImageView.image = UIImage(named: "ico_circle_unselected")
        dateAddedImage.image = UIImage(named: "ico_circle_selected")
        priceImageView.image = UIImage(named: "ico_circle_unselected")
        filterOption?.orderBy = .DateAdded
    }
    
    @IBAction func priceActionPressed(_ sender: Any) {
        downLoadImageView.image = UIImage(named: "ico_circle_unselected")
        dateAddedImage.image = UIImage(named: "ico_circle_unselected")
        priceImageView.image = UIImage(named: "ico_circle_selected")
        filterOption?.orderBy = .Price
        existFilter = true
    }
    
    @IBAction func rattingButtonsActionPressed(_ sender: UIButton) {
        existFilter = true
        switch sender {
        case rating1Button:
            rating1ImageView.image = UIImage(named: "ico_checked")
            rating2ImageView.image = UIImage(named: "ico_unchecked")
            rating3ImageView.image = UIImage(named: "ico_unchecked")
            rating4ImageView.image = UIImage(named: "ico_unchecked")
            rating5ImageView.image = UIImage(named: "ico_unchecked")
            filterOption?.ratingSelected = 5
        case rating2Button:
            rating1ImageView.image = UIImage(named: "ico_unchecked")
            rating2ImageView.image = UIImage(named: "ico_checked")
            rating3ImageView.image = UIImage(named: "ico_unchecked")
            rating4ImageView.image = UIImage(named: "ico_unchecked")
            rating5ImageView.image = UIImage(named: "ico_unchecked")
            filterOption?.ratingSelected = 4
        case rating3Button:
            rating1ImageView.image = UIImage(named: "ico_unchecked")
            rating2ImageView.image = UIImage(named: "ico_unchecked")
            rating3ImageView.image = UIImage(named: "ico_checked")
            rating4ImageView.image = UIImage(named: "ico_unchecked")
            rating5ImageView.image = UIImage(named: "ico_unchecked")
            filterOption?.ratingSelected = 3
        case rating4Button:
            rating1ImageView.image = UIImage(named: "ico_unchecked")
            rating2ImageView.image = UIImage(named: "ico_unchecked")
            rating3ImageView.image = UIImage(named: "ico_unchecked")
            rating4ImageView.image = UIImage(named: "ico_checked")
            rating5ImageView.image = UIImage(named: "ico_unchecked")
            filterOption?.ratingSelected = 2
        default:
            rating1ImageView.image = UIImage(named: "ico_unchecked")
            rating2ImageView.image = UIImage(named: "ico_unchecked")
            rating3ImageView.image = UIImage(named: "ico_unchecked")
            rating4ImageView.image = UIImage(named: "ico_unchecked")
            rating5ImageView.image = UIImage(named: "ico_checked")
            filterOption?.ratingSelected = 1
        }
    }
    
    @IBAction func closePressed(_ sender: Any) {
        if !existFilter {
            FilterEntity.shared.reset()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func leftActionDidPressed(_ sender: Any) {
        filterOption?.minPriceSelected = leftSlider.value
    }
    
    @IBAction func rightActionDidPressed(_ sender: Any) {
        filterOption?.maxPriceSelected = rightSlider.value
    }
    
    @IBAction func applyActionPressed(_ sender: Any) {
        guard filterOption != nil else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        filterOption?.maxPriceSelected = rightSlider.value
        filterOption?.minPriceSelected = leftSlider.value
        presenter?.apply(options: filterOption!)
    }
}

extension FilterViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return universes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkViewCell",
                                                 for: indexPath) as! CheckTableViewCell
        cell.nameLabel.text = universes[indexPath.row]
        if universeOptions?[indexPath.row] == true {
            cell.checkImageView.image = UIImage(named: "ico_checked")
        } else {
            cell.checkImageView.image = UIImage(named: "ico_unchecked")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        existFilter = true
        universeOptions?[indexPath.row] = true
        if previousSelection != -1 {
            universeOptions?[previousSelection] = false
        }
        filterOption?.universeSelected = universes[indexPath.row]
        previousSelection = indexPath.row
        universesTableView.reloadData()
    }
}

extension FilterViewController: FilterPresenterToViewProtocol {
    func applied() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FilterViewController {
    func setupSliders() {
        var prices = [Float]()
        productList?.results.forEach({ (result) in
            let value = result.price.replacingOccurrences(of: ",", with: ".")
            prices.append(Float(value)!)
        })
        let minValue = prices.min()
        let maxValue = prices.max()
        
        leftSlider.maximumValue = maxValue!
        leftSlider.minimumValue = minValue!
        rightSlider.maximumValue = maxValue!
        rightSlider.minimumValue = minValue!
        
        leftSlider.value = minValue!
        rightSlider.value = maxValue!
        
        minLabel.text = String(format: "$%.02f", minValue!)
        maxLabel.text = String(format: "%.02f", maxValue!)
    }
}
