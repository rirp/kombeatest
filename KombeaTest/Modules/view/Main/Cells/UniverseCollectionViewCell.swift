//
//  UniverseCell.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//
import UIKit

class UniverseCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var universeButton: DesignableLabel!
    var isEnabledValue: Bool! {
        didSet {
            universeButton.layer.cornerRadius = 5.0
            universeButton.clipsToBounds = true
            if (isEnabledValue) {
                universeButton.backgroundColor = Constant.ACCENT_COLOR
                universeButton.textColor = UIColor.white
            } else {
                universeButton.backgroundColor = UIColor.clear
                universeButton.textColor = Constant.ACCENT_COLOR
            }
        }
    }
}
