//
//  ProductCollectionViewCell.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backgroundProductImageView: UIImageView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameProductLabel: UILabel!
    @IBOutlet weak var universeProductLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    var product: Result? {
        didSet {
            if (product?.isNew)! {
                backgroundProductImageView.borderWidth = 1.0
            } else {
                backgroundProductImageView.borderWidth = 0.0
            }
            nameProductLabel.text = product?.name.uppercased()
            universeProductLabel.text = product?.universe.uppercased()
            guard product != nil, let url = URL(string: (product?.imageURL)!) else {
                return
            }
            if product?.imageData != nil {
                productImageView.image = UIImage(data: (product?.imageData)!)
            }
            self.showLoading()
            getData(from: url) { data, response, error in
                 guard let data = data, error == nil else { return }
                DispatchQueue.main.async() {
                    self.removeLoading()
                    self.productImageView.image = UIImage(data: data)
                }
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    private func showLoading() {
        self.activityIndicator.startAnimating()
        self.loadingView.isHidden = false
        self.productImageView.isHidden = true
    }
    
    private func removeLoading() {
        self.activityIndicator.stopAnimating()
        self.loadingView.isHidden = true
        self.productImageView.isHidden = false
    }
}
