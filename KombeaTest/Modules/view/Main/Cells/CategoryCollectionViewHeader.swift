//
//  CategoryCollectionViewHeader.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class CategoryCollectionViewHeader: UICollectionReusableView {
    @IBOutlet weak var headerLabel: UILabel!
}
