//
//  MainViewController.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/1/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController {
    var presenter: MainViewToPresenterProtocol?
    private var universes: [String]?
    private var universeSelectedList: [Bool] = [Bool]()
    private var productList: [Products]?
    private var universeSelected: String = NSLocalizedString("main.header.all", comment: "")
    private var indexPath: IndexPath?
    private var isRefreshed: Bool = false
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MainViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
    
    @IBOutlet weak var heightConstraintUniverse: NSLayoutConstraint!
    @IBOutlet weak var universeCollections: UICollectionView!
    @IBOutlet weak var productsCollectionView: UICollectionView!
    
    // mark: - Lifecycle ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productsCollectionView.addSubview(self.refreshControl)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "",
                                                           style: .plain,
                                                           target: nil,
                                                           action: nil)
        if (FilterEntity.shared.productFiltered != nil) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Clear Filters",
                                                                     style: .plain,
                                                                     target: self,
                                                                     action: #selector(resetAction))
            self.navigationItem.rightBarButtonItem?.tintColor = Constant.ACCENT_COLOR
        } else {
            let img = UIImage(named: "ico_fiilter")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            let rightButton = UIBarButtonItem(image: img,
                                              style: UIBarButtonItemStyle.plain,
                                              target: self,
                                              action: #selector(self.addTapped))
            rightButton.tintColor = UIColor.black
            self.navigationItem.rightBarButtonItem = rightButton
        }
        
        if productList == nil ||
            (productList?.isEmpty)! ||
            FilterEntity.shared.productFiltered != nil {
            self.universeSelected = NSLocalizedString("main.header.all", comment: "")
            presenter?.updateView()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is DetailViewController {
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.product = self.productList?[(indexPath?.section)!].results[(indexPath?.row)!]
        }
    }
    
    override func tryAgainAction() {
        self.universeSelected = NSLocalizedString("main.header.all", comment: "")
        self.presenter?.updateView()
    }
    
    @objc func addTapped(sender: UIBarButtonItem) {
        guard var product = productList else {
            return
        }
        product = (presenter?.filterProductsByUniverse(universe: NSLocalizedString("main.header.all", comment: "")))!
        productsCollectionView.reloadData()
        universeCollections.reloadData()
        FilterEntity.shared.productFiltered = product.last
        self.goToViewController(viewController: FilterRouter.createFilterViewController(product: (productList?.last!)!,
                                                                                        universes: universes!)!)
    }
    
    @objc func resetAction(sender: UIBarButtonItem) {
        FilterEntity.shared.reset()
        let img = UIImage(named: "ico_fiilter")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
        let rightButton = UIBarButtonItem(image: img,
                                          style: UIBarButtonItemStyle.plain,
                                          target: self,
                                          action: #selector(self.addTapped))
        self.navigationItem.rightBarButtonItem = rightButton
        presenter?.updateView()
    }
}

extension MainViewController: MainPresenterToViewProtocol {
    func hiddenUniverseView() {
        heightConstraintUniverse.constant = 0
        universeCollections.isHidden = true
    }
    
    func showUniverseView() {
        heightConstraintUniverse.constant = 56
        universeCollections.isHidden = false
    }
    
    func showError(message: String) {
        self.showMessageError(message: message)
    }
    
    func showLoading() {
        self.showLoadingView()
    }
    
    func removeLoading() {
        self.removeLoadingView()
    }
    
    func showProduct(products: Products) {
        self.universes = ProductFilter.getUniverses(products: products)
        universeSelectedList = [Bool]()
        if let count = universes?.count {
            for i in 1...count {
                if i == 1 {
                    universeSelectedList.append(true)
                } else {
                    universeSelectedList.append(false)
                }
            }
        }
        self.productList = presenter?.filterProductsByUniverse(universe: self.universeSelected)
        productsCollectionView.reloadData()
        universeCollections.reloadData()
    }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if (collectionView.isEqual(productsCollectionView)) {
            guard let sectionNumber = productList?.count else {
                return 0
            }
            return sectionNumber
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView.isEqual(universeCollections)) {
            guard let countUniverse = universes?.count else {
                return 0
            }
            return countUniverse
        } else {
            guard let countProductList = productList?[section].results.count else {
                return 0
            }
            return countProductList
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.isEqual(productsCollectionView) {
            let productCell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as? ProductCollectionViewCell
            productCell?.product = productList?[indexPath.section].results[indexPath.row]
            return productCell!
        } else {
            let universeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "universeCell", for: indexPath) as! UniverseCollectionViewCell
            universeCell.universeButton.text = universes?[indexPath.row]
            universeCell.isEnabledValue = universeSelectedList[indexPath.row]
            return universeCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                               withReuseIdentifier: "categoryHeader",
                                                                               for: indexPath) as? CategoryCollectionViewHeader {
            guard let count = productList?[indexPath.section].results.count else {
                return UICollectionReusableView()
            }
            let headers = getHEaders(sectionNumber: (productList?.count)!)
            sectionHeader.headerLabel.text = "\(headers[indexPath.section]) (\(count))"
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.isEqual(self.universeCollections) {
            for i in 0..<universeSelectedList.count {
                universeSelectedList[i] = i == indexPath.row
            }
            guard let universe = universes?[indexPath.row] else {
                return
            }
            universeSelected = universe
            self.productList = presenter?.filterProductsByUniverse(universe: universe)
            productsCollectionView.reloadData()
            universeCollections.reloadData()
        } else {
            self.indexPath = indexPath
            if let cell = collectionView.cellForItem(at: indexPath) as? ProductCollectionViewCell {
                self.productList?[indexPath.section].results[indexPath.row] = cell.product!
            }
            self.performSegue(withIdentifier: "detailVC", sender: self)
        }
    }
    
    private func getHEaders(sectionNumber: Int) -> [String] {
        var headers = [String]()
        switch sectionNumber {
        case 1:
            headers = ["Filtered"]
        case 2:
            headers = ["New", "All"]
        default:
            headers = ["New", "Popular", "All"]
        }
        return headers
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.universeSelected = NSLocalizedString("main.header.all", comment: "")
        self.presenter?.updateView()
        refreshControl.endRefreshing()
    }
}


extension MainViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 2 {
            let padding: CGFloat = 25
            let collectionCellSize = (collectionView.frame.size.width - padding) / 2
            let paddingToSet = collectionView.frame.size.width - (collectionCellSize * 2)
            UIEdgeInsetsMake(paddingToSet, paddingToSet, paddingToSet, paddingToSet)
        }
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 25
        let collectionCellSize = collectionView.frame.size.width - padding
        if collectionView.isEqual(productsCollectionView) {
            if (indexPath.section == 2) {
                return CGSize(width: collectionCellSize/2, height: collectionCellSize/2)
            } else {
                return CGSize(width: collectionCellSize/3, height: collectionCellSize/3)
            }
        }

        return CGSize(width: 135, height: collectionView.bounds.height)
    }

}
