//
//  StepViewController.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/1/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class StepViewController: UIViewController {
    
    
    @IBOutlet weak var nextStartButton: UIButton!
    @IBOutlet weak var stepImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    var data: WalkthroughtEntity!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        descriptionLabel.adjustsFontSizeToFitWidth = true
        descriptionLabel.minimumScaleFactor = 0.5
        stepImage.image = UIImage(named: data.image)
        descriptionLabel.text = data.stepDescription
        nextStartButton.setTitle(data.buttonText, for: .normal)
        if data.index == 2 {
            nextStartButton.backgroundColor = Constant.ACCENT_COLOR
        } else {
            nextStartButton.backgroundColor = UIColor.clear
        }
    }
    @IBAction func nextStartPressed(_ sender: Any) {
        NotificationCenter.default.post(name: Constant.NOTIFICATION_MAIN, object: nil)
    }
}
