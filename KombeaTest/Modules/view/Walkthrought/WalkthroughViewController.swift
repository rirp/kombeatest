//
//  RootViewController.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/1/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class WalkthroughViewController: UIPageViewController {
    var currentPageIndex: Int = 0
    var nextPageIndex: Int = 0
    
    var presenter: WalkthroughViewToPresenterProtocol?
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [createViewController(index: 0),
                createViewController(index: 1),
                createViewController(index: 2)]
    }()
    
    lazy var descriptions: [String] = {
        return  [NSLocalizedString("walkthrought.message.step.1", comment: ""),
                 NSLocalizedString("walkthrought.message.step.2", comment: ""),
                 NSLocalizedString("walkthrought.message.step.3", comment: "")]
    }()
    
    lazy var textButtons: [String] = {
       return [NSLocalizedString("walkthrought.button.next", comment: ""),
               NSLocalizedString("walkthrought.button.next", comment: ""),
               NSLocalizedString("walkthrought.button.get_stared", comment: "")]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createPageViewController()
        startPage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.alarmFired(_:)),
            name: Constant.NOTIFICATION_MAIN,
            object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func alarmFired(_ notification: Notification) {
        if currentPageIndex != 2 {
            goToNext()
        } else {
            presenter?.finsihWalkthrought()
        }
    }
    
    func goToNext() {
        if currentPageIndex < 3 {
            currentPageIndex += 1
        }
        if let stepViewController = orderedViewControllers[self.currentPageIndex] as? StepViewController {
            self.setViewControllers([stepViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    func createViewController(index: Int) -> UIViewController {
        let stepViewController = UIStoryboard(name: "Walkthrought", bundle: nil).instantiateViewController(withIdentifier: "step") as! StepViewController
        let data = WalkthroughtEntity()
        data.image = "ico_walkthrough\(index + 1)"
        data.buttonText = textButtons[index]
        data.stepDescription = descriptions[index]
        data.index = index
        stepViewController.data = data
        return stepViewController
    }
}

extension WalkthroughViewController: WalkthroughPresenterToViewProtocol {
    func walkthroughtFinished(viewController: UIViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension WalkthroughViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        let stepVieWcontroller = pendingViewControllers.first
        self.nextPageIndex = self.orderedViewControllers.index(of: stepVieWcontroller!)!
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            self.currentPageIndex = self.nextPageIndex
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        guard orderedViewControllers.count != nextIndex,
            orderedViewControllers.count > nextIndex,
            orderedViewControllers.count > nextIndex else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previous = viewControllerIndex - 1
        guard previous >= 0, orderedViewControllers.count > previous else {
            return nil
        }

        return orderedViewControllers[previous]
    }
    
    func createPageViewController() {
        self.dataSource = self
        self.delegate = self
    }
    
    func startPage() {
        if let firstViewController = orderedViewControllers.first {
            self.setViewControllers([firstViewController],
                                                   direction: .forward,
                                                   animated: true,
                                                   completion: nil)
        }
    }
}
