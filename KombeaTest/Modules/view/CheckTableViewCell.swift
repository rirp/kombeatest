//
//  FilterRatingViewCell.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/4/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class CheckTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
}
