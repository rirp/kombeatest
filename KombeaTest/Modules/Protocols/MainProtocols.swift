//
//  MainProtocols.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import UIKit

protocol MainPresenterToViewProtocol: class {
    func showProduct(products: Products)
    func showError(message: String)
    func showLoading()
    func removeLoading()
    func goToViewController(viewController: UIViewController)
    func hiddenUniverseView()
    func showUniverseView()
}

protocol MainViewToPresenterProtocol: class {
    var view: MainPresenterToViewProtocol? {get set};
    var interector: MainPresentorToInterectorProtocol? {get set};
    var router: MainPresenterToRouterProtocol? {get set}
    func updateView()
    func filterProductsByUniverse(universe: String) -> [Products]?
    func updateAllProduct(product: Products)
}

protocol MainInterectorToPresenterProtocol {
    func productsFetched(products: Products)
    func productsFetchedFailed()
}

protocol MainPresentorToInterectorProtocol: class {
    var presenter: MainInterectorToPresenterProtocol? {get set}
    func fetchGetProducts()
    func getIsWalkthroughShowed() -> Bool
    func getDataSaved()
}

protocol MainPresenterToRouterProtocol: class {
    func goToWalkthrough()
    static func createModule() -> UIViewController
}
