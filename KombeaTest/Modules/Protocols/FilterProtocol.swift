//
//  FilterProtocols.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import UIKit

protocol FilterPresenterToViewProtocol {
    func applied()
}

protocol FilterViewToPresenterProtocol: class {
    var view: FilterPresenterToViewProtocol? {get set};
    var interector: FilterPresentorToInterectorProtocol? {get set};
    var router: FilterPresenterToRouterProtocol? {get set}
    func apply(options: FilterEntity)
}

protocol FilterInterectorToPresenterProtocol {
}

protocol FilterPresentorToInterectorProtocol: class {
    var presenter: FilterInterectorToPresenterProtocol? {get set}
}

protocol FilterPresenterToRouterProtocol: class {
    static func createModule() -> UIViewController
}
