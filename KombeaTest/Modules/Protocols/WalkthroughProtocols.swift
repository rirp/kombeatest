//
//  Walkthrough.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import UIKit

protocol WalkthroughtPresenterToRouterProtocol: class {
    static func getWalkthroughViewController() -> UIViewController?
}

protocol WalkthroughViewToPresenterProtocol: class {
    func finsihWalkthrought()
}

protocol WalkthroughPresenterToViewProtocol: class {
    func walkthroughtFinished(viewController: UIViewController)
}

protocol WalkthroughInterectorToPresenterProtocol: class {
    
}

protocol WalkthroughPresenterToInterectorProtocol: class {
    
}
