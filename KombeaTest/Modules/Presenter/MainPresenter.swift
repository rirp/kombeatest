//
//  MainPresenter.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import UIKit

class MainPresenter: MainViewToPresenterProtocol {
    private var products: Products?
    var view: MainPresenterToViewProtocol?
    var interector: MainPresentorToInterectorProtocol?
    var router: MainPresenterToRouterProtocol?
    
    func updateView() {
        if let product = FilterEntity.shared.productFiltered {
            view?.showProduct(products: product)
        } else if (!interector!.getIsWalkthroughShowed()) {
            view?.goToViewController(viewController: WalkthroughtRouter.getWalkthroughViewController()!)
        } else {
            view?.showLoading()
            interector?.fetchGetProducts()
        }
    }
    
    func filterProductsByUniverse(universe: String) -> [Products]? {
        var product = [Products]()
        guard self.products != nil else {
            return nil
        }
        if let producFilterd = FilterEntity.shared.productFiltered {
            view?.hiddenUniverseView()
            product.append(producFilterd)
        } else if universe == NSLocalizedString("main.header.all", comment: "") {
            view?.showUniverseView()
            return ProductFilter.resetFilter(products: self.products!)
        }  else {
            view?.showUniverseView()
            let productFiltered = ProductFilter.getUniverseByName(products: self.products!, name: universe)
            let newProducts = ProductFilter.getFirstFiveNews(products: productFiltered)
            product.append(newProducts)
            product.append(productFiltered)
        }
        return product
    }
    
    private func getImageFromCell(cell: [ProductCollectionViewCell]) {
        for  (index, _) in (products?.results.enumerated())! {
            if cell[index].productImageView.isHidden == true {
                products?.results[index].imageData = Data()
            } else {
                products?.results[index].imageData = UIImagePNGRepresentation(cell[index].productImageView.image!)
            }
        }
    }
    
    func updateAllProduct(product: Products) {
        products = product
    }
}

extension MainPresenter: MainInterectorToPresenterProtocol {
    func productsFetched(products: Products) {
        self.products = products
        view?.removeLoading()
        view?.showProduct(products: products)
    }
    
    func productsFetchedFailed() {
        view?.removeLoading()
        view?.showError(message: "")
    }
}
