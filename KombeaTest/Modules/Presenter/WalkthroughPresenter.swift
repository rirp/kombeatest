//
//  WalkthroughPresenter.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

class WalkthroughPresenter: WalkthroughViewToPresenterProtocol {
    var view: WalkthroughPresenterToViewProtocol?
    var interector: WalkthroughPresenterToInterectorProtocol?
    var router: WalkthroughtPresenterToRouterProtocol?
    
    func finsihWalkthrought() {
        StorePreferences.wasWalkthroughSeen()
        view?.walkthroughtFinished(viewController: MainRouter.createModule())
    }
}

