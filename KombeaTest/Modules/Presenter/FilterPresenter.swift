//
//  FilterPresenter.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/6/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

class FilterPresenter: FilterViewToPresenterProtocol {
    var view: FilterPresenterToViewProtocol?
    var interector: FilterPresentorToInterectorProtocol?
    var router: FilterPresenterToRouterProtocol?
    
    func apply(options: FilterEntity) {
        FilterEntity.shared.setData(entity: options)
        guard FilterEntity.shared.productFiltered != nil else {
            view?.applied()
            return
        }
        var produc: Products? = orderBy(products: FilterEntity.shared.productFiltered!,
                   orderBy: FilterEntity.shared.orderBy)
        if let rating =  FilterEntity.shared.ratingSelected, rating != -1 {
            produc = getRating(products: FilterEntity.shared.productFiltered!,
                                                            rating: rating)
        }
        if let universe = FilterEntity.shared.universeSelected {
            produc = getUniverse(products: FilterEntity.shared.productFiltered!, universe: universe)
        }
        if produc != nil, (produc?.results.count != FilterEntity.shared.productFiltered?.results.count ||
            FilterEntity.shared.orderBy != .None) {
            FilterEntity.shared.productFiltered = produc
        } else {
            FilterEntity.shared.productFiltered = nil
        }
        view?.applied()
    }
    
    func orderBy(products: Products, orderBy: FilterEntity.OrderBy) -> Products {
        switch orderBy {
        case .Price:
            return ProductFilter.getByRange(products: products,
                                            minValue: FilterEntity.shared.minPriceSelected,
                                            maxValue: FilterEntity.shared.maxPriceSelected)
        case .DateAdded:
            return ProductFilter.orderByDate(products: products)!
        case .DownLoads:
            return ProductFilter.orderByDownload(products: products)!
        default:
            return products
        }
    }
    
    func getRating(products: Products, rating: Int) -> Products {
        var results = [Result]()
        products.results.forEach { (result) in
            if result.rating == String(rating) {
                results.append(result)
            }
        }
        if results.count > 0 {
            return Products(results: results)
        }
        return Products(results: results)
    }
    
    func getUniverse(products: Products, universe: String) -> Products {
        var results = [Result]()
        products.results.forEach { (result) in
            if result.universe == universe {
                results.append(result)
            }
        }
        if results.count > 0 {
            return Products(results: results)
        }
        return products
    }
}

extension FilterPresenter: FilterPresenterToViewProtocol {
    func applied() {
        
    }
}


extension FilterPresenter: FilterInterectorToPresenterProtocol {

}
