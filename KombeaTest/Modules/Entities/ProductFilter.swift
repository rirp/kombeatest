//
//  ProductFilter.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

class ProductFilter {
    static func getUniverses(products: Products) -> [String] {
        let universes = Dictionary(grouping: products.results) { (result) -> String in
            return result.universe
        }
        var universesSorted = universes.keys.sorted()
        universesSorted.insert(NSLocalizedString("main.header.all", comment: ""), at: 0)
        return universesSorted
    }
    
    static func orderByDate(products: Products?) -> Products? {
        guard var productMutable = products else {
            return nil
        }
        productMutable.results.sort(by: {
            $0.createdAt.compare($1.createdAt) == .orderedDescending
        })
        return products
    }
    
    static func getFirstFiveNews(products: Products) -> Products {
        var newProducts = ProductFilter
            .orderByDate(products: products)!
            .results
            .getFirstElements(upTo: 5)
        for (index, _) in newProducts.enumerated() {
            newProducts[index].isNew = true
        }
        return Products(results: newProducts)
    }
    
    static func getByRange(products: Products, minValue: (Float), maxValue: (Float)) -> Products {
        var result = [Result]()
        for (index, _) in products.results.enumerated() {
            let price = products.results[index].price.replacingOccurrences(of: ",", with: ".")
            if let value = Float(price), value >= minValue && value <= maxValue {
                result.append(products.results[index])
            }
        }
        return Products(results: result)
    }
    
    static func orderByPrice(products: Products?) -> Products? {
        guard var productMutable = products else {
            return nil
        }
        productMutable.results.sort(by: {
            $0.price.compare($1.price) == .orderedDescending
        })
        return productMutable
    }
    
    static func orderByDownload(products: Products?) -> Products? {
        guard var productMutable = products else {
            return nil
        }
        productMutable.results.sort(by: {
            $0.downloads.compare($1.downloads) == .orderedDescending
        })
        return productMutable
    }
    
    static func getUniverseByName(products: Products, name: String) -> Products {
        var result = [Result]()
        products.results.forEach { (resultValue) in
            if resultValue.universe == name {
                result.append(resultValue)
            }
        }
        return Products(results: result)
    }
    
    static func getPopular(products: Products) -> Products {
        var result = [Result]()
        products.results.forEach { (resultValue) in
            if resultValue.popular == true {
                result.append(resultValue)
            }
        }
        return Products(results: result)
    }
    
    static func resetFilter(products: Products) -> [Products] {
        var productList = [Products]()
        productList.append(getFirstFiveNews(products: products))
        productList.append(ProductFilter.getPopular(products: products))
        productList.append(products)
        return productList
    }
}
