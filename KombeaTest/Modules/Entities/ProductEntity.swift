//
//  ProductEntity.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

struct Products: Codable {
    var results: [Result]
}

struct Result: Codable {
    var categoryName: String?
    var imageData: Data?
    var isNew: Bool = false
    let objectID, name, createdAt, updatedAt: String
    var price: String = ""
    var imageURL: String?
    let popular: Bool
    let rating, downloads, description, sku: String
    let universe, kind: String
    
    enum CodingKeys: String, CodingKey {
        case objectID = "objectId"
        case name, createdAt, updatedAt, price, imageURL, popular, rating, downloads, description
        case sku = "SKU"
        case universe, kind
    }
}
