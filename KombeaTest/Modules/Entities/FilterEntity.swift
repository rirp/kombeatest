//
//  FilterEntity.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/6/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

class FilterEntity {
    static let shared = FilterEntity()
    var productFiltered: Products?
    
    enum OrderBy {
        case DownLoads
        case DateAdded
        case Price
        case None
    }
    
    var universeSelected: String?
    var ratingSelected: Int?
    var orderBy: OrderBy
    var minPriceSelected: Float = -1.0
    var maxPriceSelected: Float = -1.0
    
    init() {
        universeSelected = nil
        ratingSelected = nil
        orderBy = .None
    }
    
    func setData(entity: FilterEntity) {
        universeSelected = entity.universeSelected
        ratingSelected = entity.ratingSelected
        orderBy = entity.orderBy
        minPriceSelected = entity.minPriceSelected
        maxPriceSelected = entity.maxPriceSelected
    }

    func reset() {
        productFiltered = nil
        universeSelected = nil
        ratingSelected = nil
        orderBy = .None
        minPriceSelected = -1.0
        maxPriceSelected = -1.0
    }
}
