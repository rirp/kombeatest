//
//  WalkthroughtEntity.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/1/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class WalkthroughtEntity {
    var image: String = ""
    var stepDescription: String = ""
    var buttonText: String = ""
    var index: Int = 0
}
