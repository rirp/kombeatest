//
//  MainInterector.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import Alamofire

class MainInterector: MainPresentorToInterectorProtocol {
    var presenter: MainInterectorToPresenterProtocol?
    lazy var headers: HTTPHeaders = [
        Constant.API_KEY: Constant.API_KEY_VALUE,
        Constant.APPLICATION_ID: Constant.APPLICATION_ID_VALUE
    ]
    
    func fetchGetProducts() {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 15
        manager.request(Constant.BASE_URL + Constant.GET_PRUDUCT, headers: headers).responseJSON { response in
                if response.response?.statusCode == Constant.STATUS_CODE_SUCCESFUL {
                    self.fetchSuccess(data: response.data!)
                } else {
                    self.getDataSaved()
                }
        }
    }
    
    func getIsWalkthroughShowed() -> Bool {
        let userDefault = UserDefaults.standard
        return userDefault.bool(forKey: Constant.WALKTHROUGH_SHOWED)
    }
    
    private func fetchSuccess(data: Data) {
        do {
            let products = try JSONDecoder().decode(Products.self, from: data)
            StorePreferences.saveProducts(products: data)
            self.presenter?.productsFetched(products: products)
        } catch {
            print(error)
            self.getDataSaved()
        }
    }
    
    func getDataSaved() {
        if let data = StorePreferences.getProducts() {
            self.presenter?.productsFetched(products: data)
        } else {
            self.presenter?.productsFetchedFailed()
        }
    }
}
