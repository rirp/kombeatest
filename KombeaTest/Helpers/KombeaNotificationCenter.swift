//
//  KombeaNotificationCenter.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/4/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation


class KombeaNotificationCenter {
    static let shared = KombeaNotificationCenter()
    
    func notifyAction() {
        NotificationCenter.default.post(name: Constant.NOTIFICATION_MAIN, object: nil)
    }
}
