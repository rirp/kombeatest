//
//  ArrayExtension.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

extension Array {
    func getFirstElements(upTo position: Int) -> Array<Element> {
        var positionNew = position
        if position > self.count {
            positionNew = self.count
        }
        let arraySlice = self[0 ..< positionNew]
        return Array(arraySlice)
    }
}
