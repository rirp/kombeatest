//
//  StorePreferences.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

class StorePreferences {
    static func isWalkthroughSeen() -> Bool {
        let userDefault = UserDefaults.standard
        return userDefault.bool(forKey: Constant.WALKTHROUGH_SHOWED)
    }
    
    static func wasWalkthroughSeen() {
        let userDefault = UserDefaults.standard
        userDefault.set(true, forKey: Constant.WALKTHROUGH_SHOWED)
    }
    
    static func saveProducts(products: Data) {
        let userDefault = UserDefaults.standard
        userDefault.set(products, forKey: "products")
    }
    
    static func getProducts() -> Products? {
        let userDefault = UserDefaults.standard
        if let data = userDefault.data(forKey: "products") {
            do {
                return try JSONDecoder().decode(Products.self, from: data)
            } catch {
                print(error)
                return nil
            }
        } else {
            return nil
        }
    }
}
