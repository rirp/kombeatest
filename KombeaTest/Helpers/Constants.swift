//
//  Constants.swift
//  KombeaTest
//
//  Created by Ronald Ivan Ruiz Poveda on 9/2/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import UIKit

class Constant {
    // MARK: - NETWORKING
    public static let STATUS_CODE_SUCCESFUL = 200
    public static let BASE_URL = "https://parseapi.back4app.com/"
    public static let GET_PRUDUCT = "classes/Product"
    public static let APPLICATION_ID = "X-Parse-Application-Id"
    public static let APPLICATION_ID_VALUE = "I9pG8SLhTzFA0ImFkXsEvQfXMYyn0MgDBNg10Aps"
    public static let API_KEY = "X-Parse-REST-API-Key"
    public static let API_KEY_VALUE = "Yvd2eK2LODfwVmkjQVNzFXwd3N0X7oUuwiMI3VDZ"
    
    public static let WALKTHROUGH_SHOWED = "walkthroughShowed"
    
    public static let ACCENT_COLOR = UIColor(red: 219/255, green: 48/255, blue: 105/255, alpha: 1.0)
    public static let NOTIFICATION_MAIN = Notification.Name(rawValue: "WalkthroughtButtonPress")
    
    public static let MAX_RATING = 5
    
    public static let CATEGORIES = [NSLocalizedString("filter.filter.download", comment: ""),
                                  NSLocalizedString("filter.filter.date_added", comment: ""),
                                  NSLocalizedString("filter.filter.price", comment: "")]
}
