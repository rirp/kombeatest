//
//  KombeaTestTests.swift
//  KombeaTestTests
//
//  Created by Ronald Ivan Ruiz Poveda on 9/1/18.
//  Copyright © 2018 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import XCTest
@testable import KombeaTest

class KombeaTestTests: XCTestCase {
    let result = [Result(categoryName: "category",
                         imageData: nil,
                         isNew: true,
                         objectID: "",
                         name: "prueba",
                         createdAt: "prueba",
                         updatedAt: "date",
                         price: "99", imageURL: "",
                         popular: true, rating: "",
                         downloads: "", description: "adfsd",
                         sku: "",
                         universe: "Universe1", kind: "asdf"),
                  Result(categoryName: "category",
                         imageData: nil,
                         isNew: true,
                         objectID: "",
                         name: "prueba",
                         createdAt: "prueba",
                         updatedAt: "date",
                         price: "99", imageURL: "",
                         popular: true, rating: "",
                         downloads: "", description: "adfsd",
                         sku: "",
                         universe: "Universe3", kind: "asdf"),
                  Result(categoryName: "category",
                         imageData: nil,
                         isNew: true,
                         objectID: "",
                         name: "prueba",
                         createdAt: "prueba",
                         updatedAt: "date",
                         price: "99",
                         imageURL: "",
                         popular: true,
                         rating: "",
                         downloads: "",
                         description: "adfsd",
                         sku: "",
                         universe: "Universe2",
                         kind: "asdf")]
    let universeWithNull = [Result(categoryName: "category",
                                    imageData: nil,
                                    isNew: true,
                                    objectID: "",
                                    name: "prueba",
                                    createdAt: "prueba",
                                    updatedAt: "date",
                                    price: "99",
                                    imageURL: "",
                                    popular: true,
                                    rating: "",
                                    downloads: "",
                                    description: "adfsd",
                                    sku: "",
                                    universe: "",
                                    kind: "asdf")]
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testGetUniverse_Successful() {

        let products = Products(results:result)
        let universes = ProductFilter.getUniverses(products: products)
        XCTAssert(universes.count == 4) // it is 4 because add the All entry to application
    }
    
    func testGetUniverse_ExceptionValidation() {
        let products = Products(results:result)
        XCTAssertNoThrow(ProductFilter.getUniverses(products: products)) // for this momment always is true, but it must be validated to avoid erros in the future
    }
}
